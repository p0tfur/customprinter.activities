This set of activities gives your Robot ability to print documents.

Get Default Printer: returns string with default system printer name.

List Printers: returns list of <strings> with names of all installed system printers.

Print file: this activity gives you a possibility to print *.doc, *.docx, *.xls, *.xlsx, *.txt, *.ppt and *.pptx files. You can choose which printer you want to use for printing (use list printers activity to obtain name) or default system printer will be used.

This activity uses default application for printing, it means that for all these files there is needed a proper application to be installed and set as default for specific file (*.doc, *.docx => MS Word, *.xls, *.xlsx => MS Excel, *.txt = Notepad, *.ppt, *.pptx => MS PowerPoint) otherwise it will not be possible to print file.

Print PDF: activity for PDF files printing, uses Adobe Reader for prinitng, so https://get.adobe.com/pl/reader/ has to be installed and set as default application for .pdf files. You can choose which printer you want to use for printing (use list printers activity to obtain name) or default system printer will be used.

Set Default Printer: activity to set default system printer, please use 'List Printers' activity to obtain name.