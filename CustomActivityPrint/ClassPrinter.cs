﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Activities;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Drawing.Printing;
using System.Management;
using System.Runtime.InteropServices;
using System.Printing;
using Microsoft.Win32;
using RawPrint;
using PdfiumViewer;
using System.Threading;



//name of group in UiPath
namespace PrinterActivities
{
    public class List_Printers : CodeActivity
    {
        [Category("Output")]
        [Description("Returns list of installed printers")]
        [RequiredArgument]
        public OutArgument<List<string>> Output { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            List<string> output = new List<string>();

            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                //Console.WriteLine(printer);
                output.Add(printer);
            }

            Output.Set(context, output);
        }
    }

    public class Set_Default_Printer : CodeActivity
    {
        [Category("Input")]
        [Description("Name of printer")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool SetDefaultPrinter(string Name);

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            SetDefaultPrinter(input);

        }
    }

    public class Get_Default_Printer : CodeActivity
    {
        [Category("Output")]
        [Description("Returns name of default system printer")]
        [RequiredArgument]
        public OutArgument<string> Output { get; set; }

       
        protected override void Execute(CodeActivityContext context)
        {
            string output = Output.Get(context);
            PrinterSettings settings = new PrinterSettings();
            output = settings.PrinterName;
            Output.Set(context, output);
        }
    }


    //dependency https://get.adobe.com/pl/reader/ installed and set as default app for .pdf
    public class Print_PDF_AdobeReader : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer, if not provided default will be used")]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);

            string extension = Path.GetExtension(input);

            if (extension == ".pdf" || extension == ".PDF")
            {
                PrinterSettings settings = new PrinterSettings();

                if (String.IsNullOrEmpty(printer))
                {
                    printer = settings.PrinterName;
                }

                ProcessStartInfo info = new ProcessStartInfo();
                info.Arguments = "\"" + printer + "\"";
                info.Verb = "PrintTo";
                info.FileName = @input;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();

                p.WaitForInputIdle();
                Thread.Sleep(3000);
                if (false == p.CloseMainWindow())
                    p.Kill();
            }
            else
            {
                Console.WriteLine("PrinterActivities: unsupported file type!: " + extension);
            }
        }
    }

    public class Print_PDF_AdobeReader2 : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer, if not provided default will be used")]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);

            string extension = Path.GetExtension(input);

            if (extension == ".pdf" || extension == ".PDF")
            {
                PrinterSettings settings = new PrinterSettings();

                if (String.IsNullOrEmpty(printer))
                {
                    printer = settings.PrinterName;
                }

                Process p = new Process();
                p.StartInfo = new ProcessStartInfo()
                {
                    CreateNoWindow = true,
                    Verb = "print",
                    FileName = @input //put the correct path here
                };
                p.Start();
            }
            else
            {
                Console.WriteLine("PrinterActivities: unsupported file type!: " + extension);
            }
        }
    }

    public class Print_PDF_AdobeReader3 : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer, if not provided default will be used")]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);

            string extension = Path.GetExtension(input);

            if (extension == ".pdf" || extension == ".PDF")
            {
                ProcessStartInfo info = new ProcessStartInfo();
                info.Verb = "print";
                info.FileName = @input;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                Process p = new Process();
                p.StartInfo = info;
                p.Start();

                long ticks = -1;
                while (ticks != p.TotalProcessorTime.Ticks)
                {
                    ticks = p.TotalProcessorTime.Ticks;
                    Thread.Sleep(1000);
                }

                if (false == p.CloseMainWindow())
                    p.Kill();
            }
            else
            {
                Console.WriteLine("PrinterActivities: unsupported file type!: " + extension);
            }
        }
    }

    //doc, docx, txt, xls, xlsx, ppt, pptx
    public class Print_File : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer, if not provided default will be used")]
        public InArgument<string> Printer { get; set; }

        /*
        [Category("Output")]
        [DisplayName("Job ID")]
        [Description("Returns ID of printed job (from Printer Queue)")]
        [RequiredArgument]
        public OutArgument<string> JobID { get; set; }
        */
        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);
            //string extension;

            string extension = Path.GetExtension(input);

            if (extension == ".doc" || extension == ".DOC" || extension == ".docx" || extension == ".DOCX" || extension == ".xls" || extension == ".XLS" || extension == ".xlsx" || extension == ".XLSX" || extension == ".txt" || extension == ".TXT" || extension == ".ppt" || extension == ".PPT" || extension == ".pptx" || extension == ".PPTX")
            {
                if (String.IsNullOrEmpty(printer))
                {
                    PrinterSettings settings = new PrinterSettings();
                    printer = settings.PrinterName;
                }

                ProcessStartInfo info = new ProcessStartInfo(input);
                info.Arguments = "\"" + printer + "\"";
                info.Verb = "PrintTo";
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                Process.Start(info);
            }
            else
            {
                Console.WriteLine("PrinterActivities: unsupported file type!: " + extension);
            }
        }
    }

    public class Get_Data_From_Queue : CodeActivity
    {
        [Category("Output")]
        [Description("Returns number of jobs in queue")]
        [DisplayName("No. of jobs")]
        public OutArgument<int> JobsNo { get; set; }

        [Category("Output")]
        [Description("Returns details of queue as datatable: PositionInQueue, JobIdentifier, DocName, Status, Owner, Pages, Size, Submitted")]
        [DisplayName("Queue details")]
        public OutArgument<System.Data.DataTable> QueueData { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
                //create data table
                System.Data.DataTable dt = new System.Data.DataTable();
                dt.Columns.Add("PositionInQueue");
                dt.Columns.Add("JobIdentifier");
                dt.Columns.Add("DocName");
                dt.Columns.Add("Status");
                dt.Columns.Add("Owner");
                dt.Columns.Add("Pages");
                dt.Columns.Add("Size");
                dt.Columns.Add("Submitted");
                System.Data.DataRow dr = null;
                string printerName = new PrinterSettings().PrinterName;
                LocalPrintServer localPrintServer = new LocalPrintServer();
                PrintQueueCollection printQueues = localPrintServer.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });

                if (printerName == null)
                {
                    Console.WriteLine("Get_Data_From_Queue: Cannot get printer name!");
                }
                else if (localPrintServer == null)
                {
                    Console.WriteLine("Get_Data_From_Queue: Cannot get local print server!");
                }
                else if (printQueues == null)
                {
                    Console.WriteLine("Get_Data_From_Queue: There are not any queues available!");
                }
                else
                {

                    PrintQueue queue = printQueues.Where(x => x.Name.Equals(printerName)).FirstOrDefault();
                    queue.Refresh();
                    if (queue.NumberOfJobs != 0)
                    {
                        foreach (PrintSystemJobInfo psji in queue.GetPrintJobInfoCollection())
                        {
                            //new row for each iteration
                            dr = dt.NewRow();
                            //set values
                            dr["PositionInQueue"] = psji.PositionInPrintQueue;
                            dr["JobIdentifier"] = psji.JobIdentifier;
                            dr["DocName"] = psji.Name;
                            dr["Status"] = psji.JobStatus;
                            dr["Owner"] = psji.Submitter;
                            dr["Pages"] = psji.NumberOfPages;
                            dr["Size"] = psji.JobSize;
                            dr["Submitted"] = psji.TimeJobSubmitted;
                            //add
                            dt.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Get_Data_From_Queue: There are not any jobs.");
                    }

                    JobsNo.Set(context, queue.NumberOfJobs);
                    QueueData.Set(context, dt);

                }
        }
    }

    public class Print_RawPrint : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer")]
        [RequiredArgument]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);

            try
            {
                IPrinter prntr = new Printer();
                // Print the file
                Console.WriteLine("filename: {0}", Path.GetFileName(@input));
                prntr.PrintRawFile(printer, @input);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public class Print_RawPrint2 : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer")]
        [RequiredArgument]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);
            string printer = Printer.Get(context);

            try
            {
                IPrinter prntr = new Printer();
                // Print the file
                Console.WriteLine("filename: {0}", Path.GetFileName(@input));
                prntr.PrintRawFile(printer, @input, Path.GetFileName(@input));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public class Print_Pdfiumviewer : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Printer")]
        [Description("Name of the printer")]
        [RequiredArgument]
        public InArgument<string> Printer { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string filename = Input.Get(context);
            string printer = Printer.Get(context);
            //string paperName = "A4";
            int copies = 1;
                try
                {
                    // Create the printer settings for our printer
                    var printerSettings = new PrinterSettings
                    {
                        PrinterName = printer,
                        Copies = (short)copies,
                    };

                    // Create our page settings for the paper size selected
                    var pageSettings = new PageSettings(printerSettings)
                    {
                        Margins = new Margins(0, 0, 0, 0),
                    };
                    /*
                    foreach (PaperSize paperSize in printerSettings.PaperSizes)
                    {
                        if (paperSize.PaperName == paperName)
                        {
                            pageSettings.PaperSize = paperSize;
                            break;
                        }
                    }
                    */
                    // Now print the PDF document
                    using (var document = PdfDocument.Load(filename))
                    {
                        using (var printDocument = document.CreatePrintDocument())
                        {
                            printDocument.PrinterSettings = printerSettings;
                            printDocument.DefaultPageSettings = pageSettings;
                            printDocument.PrintController = new StandardPrintController();
                            printDocument.Print();
                        }
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception(ex.Message);
                }

        }
    }

    public class Print : CodeActivity
    {
        [Category("Input")]
        [Description("Full file path")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            string input = Input.Get(context);

            try
            {
                var file = File.ReadAllBytes(@input);
                var printQueue = LocalPrintServer.GetDefaultPrintQueue();

                using (var job = printQueue.AddJob())
                using (var stream = job.JobStream)
                {
                    stream.Write(file, 0, file.Length);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}